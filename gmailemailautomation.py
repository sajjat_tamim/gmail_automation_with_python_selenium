from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd

EmailList = pd.read_excel('emaillist-aqa.xlsx', engine='openpyxl')

Email_subject = "We are giving you 1 Month Free Drinking water service for Home & Office"



def copy_content(browser):
    browser.execute_script("window.open('');")
    # Switch to the new window and open URL B
    browser.switch_to.window(browser.window_handles[1])
    browser.get('https://docs.google.com/document/d/12uxFAu7XEmbozVc429t6YplPjTS_-f8dGqSDMjg1ZV8/edit?usp=sharing')
    time.sleep(4)
    browser.implicitly_wait(20)

    webdriver.ActionChains(browser).key_down(Keys.CONTROL).send_keys("a").perform()
    time.sleep(1)
    webdriver.ActionChains(browser).key_down(Keys.CONTROL).send_keys("c").perform()
    time.sleep(1)
    webdriver.ActionChains(browser).key_up(Keys.CONTROL).perform()
    #webdriver.ActionChains(browser).send_keys("c").perform()
    time.sleep(1)

    browser.switch_to.window(browser.window_handles[0])
    afterlogin(browser)




def afterlogin(browser):

    global Message_Body
    global EmailList
    i = 0
    for i in EmailList.index:
        time.sleep(2)
        compose_btn = browser.find_element_by_css_selector('.T-I-KE')
        compose_btn.click()
        browser.implicitly_wait(50)
        time.sleep(1)
        recipent_email_list = browser.find_element_by_class_name('vO')
        time.sleep(2)
        recipent_email_list.send_keys(EmailList.loc[i]['Email'])
        print(EmailList.loc[i]['Email'])
        time.sleep(1)

        subject_bar = browser.find_element_by_class_name('aoT')
        subject_bar.send_keys(Email_subject)
        time.sleep(2)

        '''message_bar = browser.find_element_by_css_selector('#\:qg').click()
        print(message_bar)
        time.sleep(1)'''
        message_bar2 = browser.find_element_by_css_selector('.Am.Al.editable.LW-avf.tS-tW')
        message_bar2.send_keys(Keys.CONTROL, "v")

        time.sleep(1)

        send_btn = browser.find_element_by_class_name('dC').click()
        time.sleep(2)

    else:

        print("All email send completly")




def Logingmail():
    browser = webdriver.Chrome('I:\SeleniumWebdriver\chromedriver.exe')
    browser.get('https://mail.google.com')
    browser.maximize_window()

    time.sleep(1)

    Login_username = browser.find_element_by_id('identifierId')
    Login_username.send_keys('XXXXXXX@gmail.com')
    time.sleep(1)
    nxt_btn = browser.find_element_by_class_name('VfPpkd-RLmnJb').click()
    browser.implicitly_wait(10)

    Login_pass = browser.find_element_by_css_selector('#password > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)')
    Login_pass.send_keys('XXXXXX')

    time.sleep(1.5)

    Submit_button = browser.find_element_by_class_name('VfPpkd-RLmnJb')
    Submit_button.click()
    browser.implicitly_wait(20)
    time.sleep(3)

    copy_content(browser)





def main():
    Logingmail()

if __name__ == "__main__":
    main()